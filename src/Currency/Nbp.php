<?php


namespace Versedi\CurrencyRates\Currency;

/**
 * Class Nbp
 * @package Versedi\CurrencyRates\Currency
 */
class Nbp
{
	/**
	 * @var string
	 */
	public $nbpRatesUrl;
	/**
	 * @var array
	 */
	public $rates = array();

	/**
	 *
	 */
	public function __construct()
	{

		$htmlUrl = 'http://nbp.pl/Kursy/KursyA.html';
		$xmlFileName = $this->parsePageFindXmlFileName($htmlUrl);

		$this->nbpRatesUrl = 'http://www.nbp.pl/Kursy/xml/' . $xmlFileName . '.xml';
	}

	/**
	 * @param $url
	 *
	 * @return mixed
	 */
	public function parsePageFindXmlFileName($url)
	{
		$htmlUrl = file_get_contents($url);
		preg_match('/kursy\/xml\/(.*)\.xml\"/', $htmlUrl, $found);
		$xmlFileName = $found[1];
		return $xmlFileName;
	}

	/**
	 * @return array
	 * @throws Exception
	 */
	public function getRatesTable()
	{

		$ratesSimpleXml = simplexml_load_file($this->nbpRatesUrl);

		if(!$ratesSimpleXml) {
			throw new Exception('Unable to open rates XML file. Try again later or check server internet connection.');
		}

		foreach($ratesSimpleXml->pozycja as $currency) {
			$currencyCode = (string)$currency->kod_waluty;
			$this->rates[$currencyCode] = new \stdClass();
			$this->rates[$currencyCode]->currencyCode = $currencyCode;
			$this->rates[$currencyCode]->countryName = (string)$currency->nazwa_kraju;
			$this->rates[$currencyCode]->currencyName = (string)$currency->nazwa_waluty;
			$this->rates[$currencyCode]->rate = (string)$currency->kurs_sredni;
		}
		return $this->rates;
	}

	/**
	 * @param $currencyCode
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function getCurrencyRate($currencyCode)
	{
		$this->getRatesTable();
		if(!array_key_exists($currencyCode, $this->rates)) {
			throw new Exception('Rate for ' . $currencyCode . ' isn\'t available.');
	    }
		if(is_string($currencyCode)) {
			return $this->rates[$currencyCode];
		}

	}
}